#include <planning/dijkstra.hpp>

int main() {
    std::shared_ptr<Grid> grid = std::make_shared<Grid>(3, 3);
    std::shared_ptr<Robot> robot = std::make_shared<Robot>(0, 0, grid);
    Dijkstra d(robot);
    d.Find(std::make_pair(2U, 2U));
    return 0;
}