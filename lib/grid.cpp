#include <planning/grid.hpp>

Grid::Grid (const unsigned int width_, const unsigned int height_) : width(width_), height(height_) {
    data = std::make_shared<std::vector<std::unique_ptr<Node>>>();
    size = width_*height_;
    data->reserve(size);
    for (size_t j = 0; j < height; j++) {
        for (size_t i = 0; i < width; i++) {
            data->push_back(std::make_unique<Node>(i, j));
        }
    }
}

void Grid::SetObstacles(const std::vector<std::pair<uint, uint>>& obstacles) {
    for (auto obstacle : obstacles) {
        (*data)[obstacle.first + height*obstacle.second]->occupied = true;
    }
}

void Grid::GetNeighbors(std::vector<Edge>& neighbors, unsigned int idx) {
    neighbors.clear();
    std::pair<unsigned int, unsigned int> xy = IdxToXY(idx);
    Edge cursor;
    std::pair<unsigned int, unsigned int> tempxy = xy;
    for (int j = -1*(xy.second != 0); j < 2; j++) {
        for (int i = -1*(xy.first != 0); i < 2; i++) {
            if (j == 0 && i == 0) { continue; }
            tempxy.first += i;
            tempxy.second += j;
            if (InsideGrid(tempxy.first, tempxy.second)) {
                if (!IsObstacle(tempxy.first, tempxy.second)) {
                    cursor.parent = idx;
                    cursor.child = XYToIdx(tempxy);
                    if (i == 0 || j == 0) {
                        cursor.cost = 1;
                    } else {
                        cursor.cost = 1.414;
                    }
                    neighbors.push_back(cursor);
                }
            }
            tempxy = xy;
        }
    }
}