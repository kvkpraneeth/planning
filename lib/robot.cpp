#include <planning/robot.hpp>

Robot::Robot(const unsigned int x_, const unsigned int y_, 
    const std::shared_ptr<Grid>& env_) : x(x_), y(y_), env(env_) 
{}

void Robot::Move(const Moves& move) {
    if (move == Moves::TOP_LEFT) {
        x -= 1;
        y += 1;
    } else if (move == Moves::TOP) {
        y += 1;
    } else if (move == Moves::TOP_RIGHT) {
        x += 1;
        y += 1;
    } else if (move == Moves::LEFT) {
        x -= 1;
    } else if (move == Moves::RIGHT) {
        x += 1;
    } else if (move == Moves::BOTTOM_LEFT) {
        x -= 1;
        y -= 1;
    } else if (move == Moves::BOTTOM) {
        y -= 1;
    } else if (move == Moves::BOTTOM_RIGHT) {
        x += 1;
        y -= 1;
    }
}