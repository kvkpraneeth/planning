# Planning

## README Structure

1. [Installation](##Installation)
2. [Directory Structure](##Structure)

## Installation

To clone the repository to your local system:
```
git clone git@gitlab.com:kvkpraneeth/planning.git
```

To build the repository in your local system:
```
cd planning
mkdir build && cd build
cmake ..
make
```

## Structure

The structure of the project is as follows:
```
planning
├── CMakeLists.txt
├── docs
├── include
├── lib
└── src
```

What do each of these mean:

1. [CMakeLists.txt](https://youtu.be/HPMvU64RUTY) : CMake file.
2. docs: Contains md files with relevant information.
3. include: Contains all header files.
4. lib: Contains all files that compile into shared libraries.
5. src: Contains all the executables.

## More Information

For more information check out `docs/`