# API Guide

## Project Structure

```
Header Files -------------------------------> Libraries
    |                                              |
    |                                              |
    ------------------> Executables <---------------
```

## Header Files

1. `types.hpp`: It defines important types and cements notation. The following types are defined:

```
Node:
    Members: x, y, occupied;
        x: unsigned int, represents x-axis value of a node.
        y: unsigned int, represents y-axis value of a node.
        occupied: bool, represents if a node is free to occupy the robot or not.

    Constructors:
        Node(unsigned int x, unsigned int y);  // occupied is assumed to be false.
        Node(unsigned int x, unsigned int y, bool occupied);

Edge:
    Members: parent, child, cost;
        parent: unsigned int, The index of the parent in the grid array.
        child: unsigned int, The index of the child in the grid array.
        cost: double, weight of the Edge.

Moves: enum{TOP_LEFT, TOP, TOP_RIGHT, LEFT, RIGHT, BOTTOM_LEFT, BOTTOM, BOTTOM_RIGHT}
    How to access these values? 
    Moves::TOP_LEFT, Moves::TOP_RIGHT

Path: vector{pair{x,y}}
    A vector of pairs containing x and y.
    How to access these values?
        for (auto pair: vector){
            unsigned int x = pair.first
            unsigned int y = pair.second
        }
```

2. `grid.hpp`: It defines the grid ds class containing different members and functions that make a help use a grid.

```
Grid:
    Constructors:
        Grid(unsigned int width, unsigned int height);
    
    Members:
        width: unsigned int, width of the grid.
        height: unsigned int, height of the grid.
        size: unsigned int, width*height.
        data: shared_ptr<vector<unique_ptr<Node>>> 
            A row-major order representation of a 2D grid.
    
    Functions:
        SetObstacles(vector<pair<unsigned int, unsigned int>> obstacles):
            Given a vector of coordinates it marks them as occupied.
            Usage:
                Grid g(5,5);
                std::vector<std::pair<unsigned int, unsigned int>> v;
                v.reserve(2);
                v.push_back(std::make_pair(1U, 0U));
                v.push_back(std::make_pair(2U, 0U));
                g.SetObstacles(v);
        
        GetNeighbors(vector<Edge> neighbors, unsigned int idx):
            Given the index of a node in the data array it gives a vector of valid neighbors in the grid.
            Usage:
                Grid g(3,3);
                std::vector<std::pair<unsigned int, unsigned int>> v;
                v.reserve(2);
                v.push_back(std::make_pair(1U, 0U));
                v.push_back(std::make_pair(2U, 0U));
                g.SetObstacles(v);
                std::vector<Edge> n;
                g.GetNeighbors(n, 4);
                for (auto e : n) {
                    std::cout << "(" << e.parent << ", " << e.child << ")\t";
                }
                std::cout << std::endl;
        
        InsideGrid(unsigned int x, unsigned int y):
            Given x, y of a Node, returns true if it is inside the grid boundaries.
        
        IsObstacle(unsigned int x, unsigned int y):
            Given x, y of a Node, return true if it is occupied.

        IdxToXY(unsigned int idx):
            Given the index of a node in the data array it gives a pair<unsigned int, unsigned int> whose first field has x and second has y.
        
        XYToIdx(pair<unsigned int, unsigned int> xy):
            Given x, y in a pair it return the index of the element inside the data array.
```

3. `robot.hpp`: It defines the robot class.

```
Robot:
    Constructor:
        Robot(unsigned int x, unsigned int y, shared_ptr<Grid> env):
            x, y represent the position of the robot in the grid.
            env represent the grid in which the robot exists.
            Usage:
                std::shared_ptr<Grid> grid = std::make_shared<Grid>(3, 3);
                Robot r(1, 1, grid);
    
    Members:
        x: unsigned int, robot's x position.
        y: unsigned int, robot's y position.
        env: shared_ptr<Grid>, a pointer to the robot's environment.
    
    Functions:
        Move(Moves move):
            Move the robot by giving it an enum value of the movement.
            Usage:
                r.Move(Moves::TOP_LEFT);
```

4. `dijkstra.hpp`: It defines the dijkstra path planning class.

```
Dijkstra:
    Constructor:
        Dijkstra (shared_ptr<Robot> robot):
            construct the path planning class.
            Usage:
                std::shared_ptr<Grid> grid = std::make_shared<Grid>(3, 3);
                std::shared_ptr<Robot> robot = std::make_shared<Robot>(0, 0, grid);
                Dijkstra d(robot);
    
    Members:
        robot: shared_ptr<Robot>, A shared ptr to the robot. 
        env: shared_ptr<Grid>, A shared ptr to the robot.
        path: Path, path variable that needs to be filled.
    
    Functions:
        Find(pair<unsigned int, unsigned int> target): TODO
            Given the target it finds the shortest path to the target and updates the path member.
        
        PrintPath(): TODO
            Prints the path.
```