#ifndef INCLUDE_PLANNING_DIJKSTRA_HPP__
#define INCLUDE_PLANNING_DIJKSTRA_HPP__

#include <planning/robot.hpp>
#include <queue>

class Dijkstra {

 public:
    explicit Dijkstra (const std::shared_ptr<Robot>& robot_) : robot(robot_) {
        env = robot_->env;
    }

    void Find(const std::pair<unsigned int, unsigned int>& target);

    Path path;

    void PrintPath ();

 private:
    std::shared_ptr<Robot> robot;
    std::shared_ptr<Grid> env;
};

#endif