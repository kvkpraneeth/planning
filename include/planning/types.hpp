#ifndef INCLUDE_PLANNING_TYPES_HPP__
#define INCLUDE_PLANNING_TYPES_HPP__

#include <vector>
#include <queue>

typedef struct Node {
    unsigned int x;
    unsigned int y;
    bool occupied=false;

    Node (const unsigned int x_, const unsigned int y_, const bool occupied_) 
        : x(x_), y(y_), occupied(occupied_)
    {}

    Node (const unsigned int x_, const unsigned int y_) : x(x_), y(y_)
    {}

} Node;

typedef struct Edge {
    unsigned int parent;  // Parent Idx
    unsigned int child;  // Child Idx
    double cost;
} Edge;

typedef enum Moves {
    TOP_LEFT, TOP, TOP_RIGHT,
    LEFT, RIGHT,
    BOTTOM_LEFT, BOTTOM, BOTTOM_RIGHT
} Moves;

typedef std::vector<std::pair<unsigned int, unsigned int>> Path;

#endif