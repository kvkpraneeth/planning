#ifndef INCLUDE_PLANNING_GRID_HPP__
#define INCLUDE_PLANNING_GRID_HPP__

#include <iostream>
#include <vector>
#include <memory>

#include <planning/types.hpp>

class Grid {

 public:
    Grid(const unsigned int width_, const unsigned int height_);

    void SetObstacles(
        const std::vector<std::pair<unsigned int, unsigned int>>& obstacles);

    unsigned int width, height, size;

    // Row Major Order
    std::shared_ptr<std::vector<std::unique_ptr<Node>>> data;

    void GetNeighbors(std::vector<Edge>& neighbors, unsigned int idx);

    bool InsideGrid(const unsigned int x, const unsigned int y) { 
        return x < width && y < height;
    }

    bool IsObstacle(const unsigned int x, const unsigned int y) {
        return data->at(XYToIdx(std::make_pair(x, y)))->occupied;
    }

    std::pair<unsigned int, unsigned int>
    IdxToXY(unsigned int idx) {
        std::pair<unsigned int, unsigned int> xy;
        xy.second = 0;
        while (idx > width) {
            xy.second += 1;
            idx -= width;
        }
        xy.first = idx;
        return xy;
    }

    unsigned int XYToIdx(const std::pair<unsigned int, unsigned int> xy) {
        return xy.first + height*xy.second;
    }
};

#endif