#ifndef INCLUDE_PLANNING_ROBOT_HPP__
#define INCLUDE_PLANNING_ROBOT_HPP__

#include <planning/grid.hpp>

class Robot{

 public:
    
    Robot(const unsigned int x, const unsigned int y,
        const std::shared_ptr<Grid>& environment);

    void Move(const Moves& move);

    unsigned int x;
    unsigned int y;

    std::shared_ptr<Grid> env;
};

#endif